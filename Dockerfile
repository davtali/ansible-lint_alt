FROM docker.io/ansible/ansible-runner:latest


COPY config/ /home/config/
RUN pip3 install --upgrade pip \
	    && pip3 install --no-cache ansible-lint \
	    && pip3 install --no-cache requests \
        && pip3 install --no-cache anybadge \
	    && chmod +x /home/config/start.sh \
	    && chmod +x /home/config/badge.py

WORKDIR /home/project
ENTRYPOINT ["/home/config/start.sh"]
