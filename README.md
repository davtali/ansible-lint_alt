# ansible-lint_alt
![](https://gitlab.com/davtali/ansible-mongodb/-/jobs/artifacts/main/raw/ansible_warning.svg?job=ansible-lint) ![](https://gitlab.com/davtali/ansible-mongodb/-/jobs/artifacts/main/raw/ansible_failure.svg?job=ansible-lint)\
Was make by ansible-lint 5.1.2 using ansible 2.9.9

Ansible-lint alternative version \
This version create badge dynamic with anybadge, the badge print the number of warning and failure detected by ansible-lint \
The image was stored on artefact \

you can show example on the project : [here](https://gitlab.com/davtali/ansible-mongodb)

For show an alternative version to add dynamically badge ansible-lint: [here](https://gitlab.com/davtali/ansible-lint_alt)

For add badge yamllint: [here](https://gitlab.com/davtali/yamllint)


# Quick start
You must create token with scope api on gitlab on your user Setting Account (`user_settings>acces_tokens`). \
Copy your secret token, create variable (`your_project>settings>CICD>variables`) with key=TOKEN and past your token on VALUE 

Add this instruction in your .gitlab-ci.yml
```
stages:
    - lint

ansible-lint:
  stage: lint
  image:
    name: dal3ap1/ansible-lint_badge:latest
    entrypoint: [""]
  script:
    - /home/config/start.sh
  artifacts:
    paths:
      - ansible_failure.svg
      - ansible_warning.svg
    expire_in: 3 yrs
```
You can exclude rules

```
stages:
    - lint

ansible-lint:
  stage: lint
  image:
    name: dal3ap1/ansible-lint_badge:latest
    entrypoint: [""]
  variables:
    exclude: "no-changed-when,risky-file-permissions"
  script:
    - /home/config/start.sh
  artifacts:
    paths:
      - ansible_failure.svg
      - ansible_warning.svg
    expire_in: 3 yrs
```


# Version without api
Not recommanded, docker in docker was slower and you can't redirect link url without use TOKEN (for api).
The redirect is to the last pipeline
We reuse image create [here](https://gitlab.com/davtali/ansible-lint)
The redirection is to the last pipeline.
```
ansible-lint_alt:
  image: docker:latest
  stage: lint
  services:
    - docker:dind
  script:
    - err=$(docker run --rm -v $PWD:/project/playbook -e exclude=no-changed-when,risky-file-permissions,command-instead-of-shell,deprecated-bare-vars,command-instead-of-shell,ignore-errors,package-latest dal3ap1/ansible-lint:latest 2>&1 >/dev/null)
    - echo $err
    - failure=$(echo $err | { grep "Finished with" || true;} | sed -e 's/.*with \(.*\)failure.*/\1/' | sed "s,\x1B\[[0-9;]*[a-zA-Z],,g");
    - failure=${failure:-0}
    
    - warning=$(echo $err | { grep "Finished with" || true;} | sed -e 's/.*, \(.*\)warning.*/\1/' | sed "s,\x1B\[[0-9;]*[a-zA-Z],,g");
    - warning=${warning:-0}

    - docker run --rm --name py -it -d -v $PWD:/home -w /home -e failure='{$failure}' -e warning='{$warning}' --entrypoint /bin/bash python
    - docker exec py pip install anybadge
    - docker exec py anybadge -l ansible -v ${failure} -f ansible_failure.svg -c brightred
    - docker exec py anybadge -l ansible -v ${warning} -f ansible_warning.svg -c orange
    - docker kill py

  artifacts:
    paths:
      - ansible_failure.svg
      - ansible_warning.svg
    expire_in: 3 yrs
```
You must put the url badge in your settings project, replace the `path project` with your value (davtali/ansible-mongodb): \
https://gitlab.com/davtali/ansible-mongodb/-/jobs/artifacts/main/raw/ansible_warning.svg?job=ansible-lint_alt \
https://gitlab.com/davtali/ansible-mongodb/-/jobs/artifacts/main/raw/ansible_failure.svg?job=ansible-lint_alt
