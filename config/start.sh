#!/bin/bash


ansible-lint -f rich --force-color -x $exclude . 1 > err.txt 2>output.txt

# parse the output for have the number failure
# failure=$(cat output.txt| grep "Finished with " | sed -e 's/.*with \(.*\)failure.*/\1/' | sed "s,\x1B\[[0-9;]*[a-zA-Z],,g");
tmp=$(cat output.txt | grep "Finished with ")
failure=$(cat output.txt| grep "Finished with " | sed -e 's/.*with \(.*\)failure.*/\1/' | sed "s,\x1B\[[0-9;]*[a-zA-Z],,g");
failure=${failure:-0}  
if (( $failure==0 )); then
color="green"
else
color="brightred"
fi
# create badge
anybadge -l ansible_failure -v $failure -f ansible_failure.svg -c $color

warning=$(cat output.txt | grep "Finished with " | sed -e 's/.*, \(.*\)warning.*/\1/' | sed "s,\x1B\[[0-9;]*[a-zA-Z],,g");
warning=${warning:-0}
if (( $warning==0 )); then
color="green"
else
color="brightred"
fi
anybadge -l ansible_warning -v $warning -f ansible_warning.svg -c $color

# print on runner
cat err.txt
if [[ $failure+$warning -gt 0 ]];then
echo $tmp
fi

# upload or update badge
/home/config/badge.py

